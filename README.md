# dokuro-notes



# Web API architecture and its different components
## Understand the three components client, server, and database
## Know how they interact with each other to form a web API


# HTTP protocol and methods, HTTP status codes and their meaning
## Understand HTTP protocol and methods, including GET, POST, PUT, and DELETE
## Know HTTP status codes and their meanings, such as 200 OK, 400 Bad Request, and 500 Internal Server Error
RESTful APIs and principles	Understand the principles of RESTful APIs, including resources, HTTP methods, and hypermedia
	Know how to design a RESTful API using these principles
Resources and how they map to HTTP methods	Understand how resources map to HTTP methods, including GET, POST, PUT, and DELETE
	Know how to define resources in a .NET Core API project
Request and response formats, such as JSON and XML	Understand common request and response formats, such as JSON and XML
	Know how to serialize and deserialize data models using these formats
Make httpclient from backend	Understand the role of controllers in handling HTTP requests and responses
	Know how to create controllers in a .NET Core API project
Controllers and their role in handling HTTP requests and responses	Understand how routing works in a .NET Core API project
	Know how to map incoming HTTP requests to specific controllers and actions
Attribute routing and how to use it in controllers	Understand how to define and bind data models in a .NET Core API project
	Know how to perform input validation on these models
Routing and how to map incoming HTTP requests to specific controllers and actions	Understand what CRUD operations are and how they relate to a web API
	Know how to implement CRUD operations in a .NET Core API project
Data models and how to define and bind them in a .NET Core API project, input validation	Determine the appropriate HTTP verb to use for each action in your API.
	Dependency injection and how to register and inject services into controllers and actions
Actions and how to create them, map them to HTTP request methods, and handle input parameters and responses	Determine the appropriate authentication and authorization mechanisms for your API.
	Implement those mechanisms in your API, such as JWT authentication or OAuth.
CRUD operations and how to implement them in a .NET Core API	Cross-origin resource sharing (CORS) and how to configure policies in a .NET Core API project, as well as mitigate security risks
	Determine the appropriate CORS policy for your API.
Understand the commonly used HTTP verbs such as GET, POST, PUT, DELETE, and PATCH.	Configure the CORS policy in your API.
	Identify the design patterns and best practices that are appropriate for your API, such as the repository pattern, DTOs, and SOLID principles.
	Implement those patterns and practices in your API.
Understand the concept of dependency injection.	API documentation and how to create it using tools such as Swagger or OpenAPI, postman
	Determine the appropriate tool for documenting your API, such as Swagger or OpenAPI.
	Use the chosen tool to generate documentation for your API.
	Keep the documentation up-to-date as changes are made to the API.
Understand the concept of middleware and how it works in .NET Core.	Determine the appropriate testing framework for your API, such as NUnit or xUnit.
	Write unit and integration tests for your controllers and actions.
	Run the tests regularly to ensure that your API is functioning as expected.
	Deploying a .NET Core API project to different hosting platforms, including Azure, AWS, and web servers
	Know how to create an HttpClient in a .NET Core API project
Understand the importance of security in web APIs.	Understand how to use the HttpClient to make requests to external APIs
	Understand attribute routing and its advantages over convention-based routing
	Know how to use attribute routing in controllers
	Understand how actions work in a .NET Core API project
	Know how to create actions, map them to HTTP request methods, and handle input parameters and responses
Understand the concept of CORS and why it's important for web APIs.	Use HTTP verbs consistently throughout your API.
	HTTP verbs and their intended use in a .NET Core API project
	Identify the services that your controllers and actions depend on.
	Register those services with the dependency injection container in your application.
Understand the various performance optimization techniques available for web APIs.	Middleware and how to use it for logging, error handling, and authentication, storeage files with static, diagnose issues or optimize performance
	Inject those services into your controllers and actions.
	Identify the middleware components that you need for your application, such as logging, error handling, authentication, and static file serving.
Understand the importance of design patterns and best practices for web APIs.	Add the necessary middleware components to your application pipeline in the correct order.
	Configure the middleware components as needed.
	Security and how to implement authentication and authorization mechanisms, as well as secure communication and API endpoints
Understand the importance of API documentation.	Use HTTPS to secure communication with your API.
	Secure your API endpoints to prevent unauthorized access.
	Mitigate security risks associated with CORS, such as using preflight requests.
	Performance optimization techniques, such as caching, compression, and asynchronous programming
Understand the importance of testing for web APIs.	Determine which techniques are appropriate for your API.
	Implement those techniques in your API, such as caching, compression, and asynchronous programming.
	Design patterns and best practices for building maintainable and scalable APIs, such as the repository pattern, DTOs, and SOLID principles
	Testing and how to write unit and integration tests for controllers and actions, using frameworks such as NUnit and xUnit
Understand the various hosting platforms available for web APIs, such as Azure, AWS, and web servers.	Determine the appropriate hosting platform for your API.
	Deploy your API to
